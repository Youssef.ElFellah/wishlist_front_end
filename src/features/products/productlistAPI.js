import {URL, USERS_PRODUCTS} from '../../app/const'

async function productsCreate(data) {
    const url = URL + USERS_PRODUCTS + `/ceate`

    const settings = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify(data),
    }

    try {
      
        const response = await fetch(url, settings);
        return response.json()

    } catch (error) {
        return console.error(error)
    }
}


async function productsGetByUser(data) {
    const url = URL + USERS_PRODUCTS + `/findByUserId/${data}`

    const settings = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin' : '*',
        },
    }

    try {
      
        const response = await fetch(url, settings);
        return response.json()

    } catch (error) {
        return console.error(error)
    }
}

async function productsGetByWish(data) {
    const url = URL + USERS_PRODUCTS + `/findByWishId/${data}`

    const settings = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin' : '*',
        },
    }

    try {
      
        const response = await fetch(url, settings);
        return response.json()

    } catch (error) {
        return console.error(error)
    }
}


async function deleteProductById(data) {
    const url = URL + USERS_PRODUCTS + `/${data}`

    const settings = {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin' : '*',
        },
    }

    try {
      
        const response = await fetch(url, settings);
        return response.json()

    } catch (error) {
        return console.error(error)
    }
}


async function updateProductById(data) {
    const url = URL + USERS_PRODUCTS

    const settings = {
        method: 'PATCH',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify(data),
    }

    try {
      
        const response = await fetch(url, settings);
        return response.json()

    } catch (error) {
        return console.error(error)
    }
}

export {productsCreate, productsGetByUser, productsGetByWish, deleteProductById, updateProductById} 



