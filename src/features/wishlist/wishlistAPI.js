import {URL, USERS_WISHS} from '../../app/const'

async function wishlistCreate(data) {
    const url = URL + USERS_WISHS + `/ceate`

    const settings = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify(data),
    }

    try {
      
        const response = await fetch(url, settings);
        return response.json()

    } catch (error) {
        return console.error(error)
    }
}


async function wishlistGetByUser(data) {
    const url = URL + USERS_WISHS + `/findByUserId/${data}`

    const settings = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin' : '*',
        },
    }

    try {
      
        const response = await fetch(url, settings);
        return response.json()

    } catch (error) {
        return console.error(error)
    }
}


async function deleteWhishById(data) {
    const url = URL + USERS_WISHS + `/${data}`

    const settings = {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin' : '*',
        },
    }

    try {
      
        const response = await fetch(url, settings);
        return response.json()

    } catch (error) {
        return console.error(error)
    }
}


async function updateWhishById(data) {
    const url = URL + USERS_WISHS

    const settings = {
        method: 'PATCH',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify(data),
    }

    try {
      
        const response = await fetch(url, settings);
        return response.json()

    } catch (error) {
        return console.error(error)
    }
}

export {wishlistCreate, wishlistGetByUser, deleteWhishById, updateWhishById} 



