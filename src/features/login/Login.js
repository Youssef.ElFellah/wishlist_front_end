import React, { useState } from 'react'
import { useDispatch } from 'react-redux';

import { useNavigate } from "react-router-dom"

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';

import {Link} from 'react-router-dom';

import {login} from './loginSlice'
import {useAuth} from '../context/UseAuth'

import {userLogin} from './loginAPI'

const useStyles = makeStyles({
    img: {
        height: '100px',
        width: '100px',
        border: '1px solid',
        marginLeft: '46%',
        marginBottom: '5%',
        marginTop: '5%',
    },
    box: {
        height: '40%',
        width: '35%',
        border: '1px solid',
        marginLeft: '32%',
    },
    span: {
        marginLeft: '10%',
        fontSize: '16px'
    },
    input: {
        width: '85%',
    },
    link: {
        marginLeft: '10%',
        marginTop: '0px',
        textDecoration: 'none',
    },
    title: {
        fontWeight: '500',
        fontSize: '40px',
    },
    button: {
        margin: '10%',
    }
});



export function Login(props) {
    const classes = useStyles();
    const dispatch = useDispatch();
    const navigate = useNavigate();
    let auth = useAuth();

    
    const [name, setName] = useState('')
    const [password, setPassword] = useState('')

    const handleLogin = async () => {
        let data = {
            name: name,
            password: password,
        }
        try {
            
            let res = await userLogin(data)
            if(res.success) {                
                dispatch(login(res.data))
                auth.signin(() => {
                    navigate(`/`, { replace: true })
                })
            }
            
        } catch (error) {
            console.log(error.message)
        }
    }

    return (
        <Box>
            <Box className={classes.img}>
            </Box>
            <Grid
                container
                direction="row"
                justifyContent="center"
                alignItems="center"
                className={classes.box}>
                <Grid item xs={12}>
                    <span className={classes.title}>Sign In</span>
                </Grid>
                
                <Grid item xs={12}>

                    <Grid 
                        container
                        direction="row"
                        justifyContent="flex-start"
                        alignItems="center" item xs={12}>
                        <span className={classes.span}>Name</span>
                    </Grid>  
                    <Grid item xs={12}>  
                        <TextField className={classes.input} variant="outlined" value={name} onChange={event => setName(event.target.value)} />                    
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                
                    <Grid 
                        container
                        direction="row"
                        justifyContent="flex-start"
                        alignItems="center" item xs={12}>
                        <span className={classes.span}>Password</span>
                    </Grid>  
                    <Grid item xs={12}>  
                        <TextField className={classes.input}  type="password" variant="outlined" value={password} onChange={event => setPassword(event.target.value)} />                    
                    </Grid>
                </Grid>
                
                <Grid item xs={12}>
                
                    <Grid 
                        container
                        direction="row"
                        justifyContent="flex-start"
                        alignItems="center" item xs={12}>
                        <Link className={classes.link} to="/signup" variant="body2">
                            Need an account?Register
                        </Link>
                    </Grid>  

                </Grid>  
                            

                
                <Button className={classes.button} variant="contained" color="primary" onClick={handleLogin}>
                    SignIn
                </Button>


            </Grid>         
        </Box>
    )
}
